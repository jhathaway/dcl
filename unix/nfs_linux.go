package unix

import (
	"fmt"
	"net/netip"
)

func formatExport(path string, cidr netip.Prefix) string {
	return fmt.Sprintf("%s %s(ro,insecure,all_squash,no_subtree_check)", path, cidr)
}
func exportFs() error {
	err := ExecSh("sudo", "exportfs", "-rv")
	if err != nil {
		return err
	}
	return nil
}
