package unix

import (
	"errors"
	"fmt"
	"os/exec"
)

func CheckDeps() error {
	deps := []string{
		"kubectl",
		"minikube",
		"nfsd",
		"route",
		"scutil",
		"sudo",
	}
	for _, dep := range deps {
		_, err := exec.LookPath(dep)
		if err != nil {
			if errors.Is(err, exec.ErrNotFound) {
				return fmt.Errorf("Missing binary, please install, '%s'", dep)
			} else {
				return err
			}
		}
	}
	return nil
}
