package unix

import (
	"strings"
	"text/template"
)

func SplitDns(ip string, dev string, searchDomains []string) error {
	var err error
	var scutilDnsAddBuf strings.Builder
	scutilDnsAdd, err := template.ParseFS(efs, "templates/scutil_dns_add.tmpl")
	if err != nil {
		return err
	}
	data := struct {
		DnsServer        string
		DomainSearchList string
	}{
		ip,
		strings.Join(searchDomains, " "),
	}
	err = scutilDnsAdd.Execute(&scutilDnsAddBuf, data)
	if err != nil {
		return err
	}
	err = ExecShStdin(scutilDnsAddBuf.String(), "sudo", "scutil")
	if err != nil {
		return err
	}
	return nil
}
