package cfg

import (
	"fmt"
	"github.com/happy-sdk/bexp"
	"k8s.io/apimachinery/pkg/util/validation"
	"net/netip"
	"strings"
)

type DataCenter struct {
	Cidr netip.Prefix
	Name string
}

type Node struct {
	Name      string
	Subdomain string
	Codename  string
}

func (node Node) String() string {
	if node.Codename != "" {
		return fmt.Sprintf("%s.%s (debian %s)", node.Name, node.Subdomain, node.Codename)
	} else {
		return fmt.Sprintf("%s.%s", node.Name, node.Subdomain)
	}
}

type NodeSet struct {
	Desc     string
	Hosts    []string
	Codename string
}

type Images struct {
	PullPolicy     string `yaml:"pull-policy"`
	PuppetClient   string `yaml:"puppet-client"`
	PuppetServer   string `yaml:"puppet-server"`
	PkiServer      string `yaml:"pki-server"`
	PuppetDbServer string `yaml:"puppetdb-server"`
}

type NodeUnionSet struct {
	Desc string
	Sets []string
}

type Config struct {
	SourceCode    map[string]string       `yaml:"source-code"`
	NodeSets      map[string]NodeSet      `yaml:"node-sets"`
	NodeUnionSets map[string]NodeUnionSet `yaml:"node-union-sets"`
	Images        Images                  `yaml:"images"`
	PuppetEnv     string                  `yaml:"puppet-env"`
	DataCenter    DataCenter              `yaml:"data-center"`
	DnsDomain     string                  `yaml:"dns-domain"`
}

func ValidateCfgNodes(nodeStrSet []string, codename string) (nodes []Node, err error) {
	for _, domainName := range nodeStrSet {
		if ok, err := isFullyQualifiedDomainName(domainName); !ok {
			return []Node{}, fmt.Errorf("Invalid domain name, '%s', %v", domainName, err)
		}
		domainNameParts := strings.Split(domainName, ".")
		if len(domainNameParts) != 3 {
			return []Node{}, fmt.Errorf("Invalid domain name, '%s', must contain three labels", domainName)
		}
		nodes = append(nodes, Node{domainNameParts[0], domainNameParts[1], codename})
	}
	return nodes, nil
}

func isFullyQualifiedDomainName(name string) (bool, error) {
	if len(name) == 0 {
		return false, fmt.Errorf("Domain name length is 0")
	}
	if strings.HasSuffix(name, ".") {
		name = name[:len(name)-1]
	}
	if errs := validation.IsDNS1123Subdomain(name); len(errs) > 0 {
		return false, fmt.Errorf("%v", errs)
	}
	if len(strings.Split(name, ".")) < 2 {
		return false, fmt.Errorf("Should be a domain with at least two segments separated by dots, e.g. sretest1001.eqiad.wmnet")
	}
	for _, label := range strings.Split(name, ".") {
		if errs := validation.IsDNS1123Label(label); len(errs) > 0 {
			return false, fmt.Errorf("%v", errs)
		}
	}
	return true, nil
}

func LoadNodeSet(nodeSetName string, cfg Config) (nodes []Node, err error) {
	var cfgNodes []string
	if nodeSet, ok := cfg.NodeSets[nodeSetName]; ok {
		for _, v := range nodeSet.Hosts {
			cfgNodes = append(cfgNodes, bexp.Parse(v)...)
		}
	} else {
		return nil, fmt.Errorf("Node set %s, not in config\n", nodeSetName)
	}
	if nodes, err = ValidateCfgNodes(cfgNodes, cfg.NodeSets[nodeSetName].Codename); err != nil {
		return nil, err
	}
	return nodes, nil
}

func LoadUnionSet(nodeUnionSetName string, cfg Config) (nodes []Node, err error) {
	if nodeUnionSet, ok := cfg.NodeUnionSets[nodeUnionSetName]; ok {
		for _, set := range nodeUnionSet.Sets {
			ns, err := LoadNodeSet(set, cfg)
			if err != nil {
				return nil, err
			}
			nodes = append(nodes, ns...)
		}
	} else {
		return nil, fmt.Errorf("Node union set %s, not in config\n", nodeUnionSetName)
	}
	return nodes, nil
}
