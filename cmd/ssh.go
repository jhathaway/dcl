package cmd

import (
	"embed"
	"os"
	"os/exec"
	"path/filepath"

	"github.com/spf13/cobra"
)

func sshCmd() *cobra.Command {
	cmd := &cobra.Command{
		Use: "ssh",
		Short: "ssh to a given node name",
		RunE: sshToNode,
	}

	return cmd
}

var DclSshConfig embed.FS

func prepSshKeys() (string, error) {
	sshConfigDir, err := os.MkdirTemp("", "dclSshKeys")
	if err != nil {
		return "", err
	}

	sshPrivKeyFileName := filepath.Join(sshConfigDir, "dcl_cert")
	sshPrivKeyContents, err := DclSshConfig.ReadFile("ssh/id_rsa_dcl_cert")
	if err != nil {
		return "", err
	}
	err = os.WriteFile(sshPrivKeyFileName, sshPrivKeyContents, 0600)
	if err != nil {
		return "", err
	}

	sshPubKeyFileName := filepath.Join(sshConfigDir, "dcl_cert.pub")
	sshPubKeyContents, err := DclSshConfig.ReadFile("ssh/id_rsa_dcl_cert.pub")
	if err != nil {
		return "", err
	}
	err = os.WriteFile(sshPubKeyFileName, sshPubKeyContents, 0600)

	return sshPrivKeyFileName, err
}

func sshToNode(cmd *cobra.Command, args []string) error {
	sshPrivKeyFileName, err := prepSshKeys()
	if err != nil {
		return err
	}
	defer os.RemoveAll(filepath.Dir(sshPrivKeyFileName))

	sshArgs := []string{
		"-i", sshPrivKeyFileName,
		"-o", "CanonicalizeHostname yes",
		"-o", "CanonicalDomains eqiad.default.svc.k8s.lan codfw.default.svc.k8s.lan wikimedia.default.svc.k8s.lan",
		"-o", "CanonicalizeMaxDots 0",
		"-o", "IdentitiesOnly true",
	}
	sshArgs = append(sshArgs, args...)

	ssh := exec.Command("ssh", sshArgs...)
	ssh.Stdout = os.Stdout
	ssh.Stdin = os.Stdin
	ssh.Stderr = os.Stderr

	err = ssh.Run()
	return err
}