package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.wikimedia.org/jhathaway/dcl/cfg"
	"gitlab.wikimedia.org/jhathaway/dcl/kubenode"
)

func delSetCmd() *cobra.Command {
	cmd := &cobra.Command{
		Use:               "set",
		Short:             "delete a running node set",
		Args:              cobra.ExactArgs(1),
		RunE:              delSet,
		ValidArgsFunction: addSetComp,
	}
	return cmd
}

func delSet(cmd *cobra.Command, args []string) error {
	var err error
	nodeSetName := args[0]
	nodeSet, err := cfg.LoadNodeSet(nodeSetName, Cfg)
	if err != nil {
		return err
	}
	restclient, clientset, err := kubeClientset()
	if err != nil {
		return err
	}
	return kubenode.Del(nodeSet, restclient, clientset, Cfg)
}
