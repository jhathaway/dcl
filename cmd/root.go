package cmd

import (
	"errors"
	"fmt"
	"os"
	"strings"

	"github.com/adrg/xdg"
	yaml "github.com/goccy/go-yaml"
	"github.com/spf13/cobra"
	"gitlab.wikimedia.org/jhathaway/dcl/cfg"
	"gitlab.wikimedia.org/jhathaway/dcl/unix"
	"io/ioutil"
	"net/netip"
	"path/filepath"
)

// k8s imports
import (
	"k8s.io/client-go/kubernetes"
	restclient "k8s.io/client-go/rest"
	"k8s.io/client-go/tools/clientcmd"
)

var CfgPath string
var KubeCfgPath string
var Cfg cfg.Config

func expandTilde(path string) (expandedPath string) {
	if strings.HasPrefix(path, "~/") {
		home, err := os.UserHomeDir()
		if err != nil {
			panic(err)
		}
		expandedPath = filepath.Join(home, path[2:])
	} else {
		expandedPath = path
	}
	return expandedPath
}

// Assume the config file is ${XDG_CONFIG_HOME}/dcl/dcl.yaml and create some
// completions.
// TODO: look for our -f flag when using a custom config location
func specCfgLoad() error {
	var err error
	CfgPath, err = xdg.ConfigFile("dcl/dcl.yaml")
	if err != nil {
		return err
	}
	return loadCfg()
}

func preRunLoadCfg(*cobra.Command, []string) error {
	return loadCfg()
}

func loadCfg() error {
	file, err := os.Open(CfgPath)
	if err != nil {
		return err
	}
	defer file.Close()
	data, err := ioutil.ReadAll(file)
	if err != nil {
		return err
	}

	if err := yaml.Unmarshal(data, &Cfg); err != nil {
		return err
	}

	for _, repo := range []string{"puppet", "private"} {
		if repoPath, ok := Cfg.SourceCode[repo]; ok {
			if _, err := os.Stat(expandTilde(repoPath + "/.git")); errors.Is(err, os.ErrNotExist) {
				return fmt.Errorf("Path '%s' does not point to a git repository", repoPath)
			}
		} else {
			return fmt.Errorf("Must specify '%s' repository source path in config", repo)
		}
	}

	for i, src := range Cfg.SourceCode {
		Cfg.SourceCode[i] = expandTilde(src)
	}

	if Cfg.PuppetEnv == "" {
		return fmt.Errorf("Must specify puppet-env in config")
	}

	if Cfg.Images.PuppetServer == "" {
		return fmt.Errorf("Must specify a puppet-server image in config")
	}

	if Cfg.Images.PuppetClient == "" {
		return fmt.Errorf("Must specify a puppet-client image in config")
	}

	if Cfg.DataCenter.Name == "" {
		return fmt.Errorf("Must specify data-center name in config")
	}

	var zeroCidr netip.Prefix
	if Cfg.DataCenter.Cidr == zeroCidr {
		return fmt.Errorf("Must specify data-center cidr in config")
	}

	if Cfg.DnsDomain == "" {
		Cfg.DnsDomain = "k8s.lan"
	}

	if Cfg.Images.PullPolicy == "" {
		Cfg.Images.PullPolicy = "Always"
	}

	return nil
}

func kubeClientset() (*restclient.Config, *kubernetes.Clientset, error) {
	restclient, err := clientcmd.BuildConfigFromFlags("", KubeCfgPath)
	if err != nil {
		return nil, nil, err
	}
	clientset, err := kubernetes.NewForConfig(restclient)
	if err != nil {
		return nil, nil, err
	}
	return restclient, clientset, nil
}

func Execute() error {
	var err error
	var rootCmd = &cobra.Command{
		Use:           "dcl",
		Short:         "Data center in a box",
		SilenceErrors: true,
		SilenceUsage:  true,
		Long: `dcl (data center labratory) helps you spin up
your data center locally for development`,
	}

	// Add sbin to our PATH, which is needed for some dependencies, such as
	// exportfs.
	err = os.Setenv("PATH", os.Getenv("PATH")+":/sbin:/usr/sbin")
	if err != nil {
		return err
	}
	err = unix.CheckDeps()
	if err != nil {
		return err
	}

	configFilePath, err := xdg.ConfigFile("dcl/dcl.yaml")
	if err != nil {
		return err
	}
	rootCmd.PersistentFlags().StringVarP(
		&CfgPath,
		"config",
		"f",
		configFilePath,
		"config file",
	)

	homeDir, err := os.UserHomeDir()
	if err != nil {
		return err
	}
	rootCmd.PersistentFlags().StringVarP(
		&KubeCfgPath,
		"kubeconfig",
		"k",
		filepath.Join(homeDir, ".kube", "config"),
		"kubectl config file",
	)

	rootCmd.AddCommand(addCmd())
	rootCmd.AddCommand(delCmd())
	rootCmd.AddCommand(labCmd())
	rootCmd.AddCommand(listCmd())
	rootCmd.AddCommand(sshCmd())
	return rootCmd.Execute()
}
