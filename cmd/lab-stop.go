package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.wikimedia.org/jhathaway/dcl/unix"
)

func labStopCmd() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "stop",
		Short: "stop your data center lab",
		Args:  cobra.NoArgs,
		RunE:  labStop,
	}
	return cmd
}

func labStop(cmd *cobra.Command, args []string) error {
	var err error
	minikubeArgs := []string{
		"--profile=" + Cfg.DataCenter.Name,
		"stop",
	}
	err = unix.ExecSh("minikube", minikubeArgs...)
	if err != nil {
		return err
	}
	// TODO: unexport NFS code paths
	return nil
}
