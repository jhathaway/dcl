package cmd

import (
	"context"
	"fmt"
	"github.com/spf13/cobra"
	"gitlab.wikimedia.org/jhathaway/dcl/cfg"
	"gitlab.wikimedia.org/jhathaway/dcl/kubenode"
	"gitlab.wikimedia.org/jhathaway/dcl/unix"
	"math/big"
	"net/netip"
	"runtime"
	"time"
)

// k8s imports
import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
)

var Driver string

func labStartCmd() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "start",
		Short: "start your data center lab",
		Args:  cobra.OnlyValidArgs,
		RunE:  labStart,
	}
	cmd.Flags().StringVarP(&Driver, "driver", "d", "podman", "Minikube driver, podman or kvm2")
	return cmd
}

// https://stackoverflow.com/a/72903486
func splitPrefix(p netip.Prefix) (lo, hi netip.Prefix) {
	p = p.Masked()
	lo, _ = p.Addr().Prefix(p.Bits() + 1)

	delta := big.NewInt(1)
	delta.Lsh(delta, uint(lo.Addr().BitLen()-lo.Bits()))

	n := new(big.Int).SetBytes(lo.Addr().AsSlice())
	n.Add(n, delta)

	hiIP, _ := netip.AddrFromSlice(n.Bytes())
	hi = netip.PrefixFrom(hiIP, lo.Bits())

	return lo, hi
}

func getCorednsIp(clientset *kubernetes.Clientset) (string, error) {
	corednsPods, err := clientset.CoreV1().Pods("kube-system").List(
		context.Background(),
		metav1.ListOptions{
			LabelSelector: "k8s-app=kube-dns",
		},
	)
	if err != nil {
		return "", err
	}

	if len(corednsPods.Items) > 0 {
		return corednsPods.Items[0].Status.PodIP, nil
	} else {
		return "", nil
	}
}

func labStart(cmd *cobra.Command, args []string) error {
	var err error
	if Driver != "podman" && Driver != "kvm2" {
		return fmt.Errorf("Only podman and kvm2 driver supported")
	}
	err = unix.CreateDclSudoersConf()
	if err != nil {
		return err
	}
	podCidr, svcCidr := splitPrefix(Cfg.DataCenter.Cidr)
	minikubeArgs := []string{
		"start",
		"--profile=" + Cfg.DataCenter.Name,
		"--cpus=max",
		"--container-runtime=cri-o",
		"--dns-domain=" + Cfg.DnsDomain,
		"--extra-config=kubeadm.pod-network-cidr=" + podCidr.String(),
		"--service-cluster-ip-range=" + svcCidr.String(),
		"--cni=calico",
	}

	switch runtime.GOOS {
	case "linux":
		minikubeArgs = append(minikubeArgs, "--driver="+Driver)
		if Driver == "podman" || Driver == "docker" {
			minikubeArgs = append(minikubeArgs, "--memory=no-limit")
			err = unix.CreateContainersConf()
			if err != nil {
				return err
			}
			err = unix.CreatePodmanSudoersConf()
			if err != nil {
				return err
			}
		} else {
			minikubeArgs = append(minikubeArgs, "--memory=10g")
		}
	case "darwin":
		minikubeArgs = append(minikubeArgs, "--driver=qemu2", "--network=socket_vmnet", "--memory=10g")
	}

	err = unix.ExecSh("minikube", minikubeArgs...)
	if err != nil {
		return err
	}
	_, clientset, err := kubeClientset()
	if err != nil {
		return err
	}
	corednsIp := ""
	fmt.Printf("Waiting for coredns to come up")
	for corednsIp == "" {
		corednsIp, err = getCorednsIp(clientset)
		if err != nil {
			return err
		}
		fmt.Printf(".")
		time.Sleep(1 * time.Second)
	}
	fmt.Printf("\n")
	minikubeIp, err := unix.GetMinikubeIp(Cfg.DataCenter.Name)
	if err != nil {
		return err
	}
	dcRoute, err := unix.ShowRoute(Cfg.DataCenter.Cidr)
	if err != nil {
		return err
	}
	if dcRoute.Gateway != minikubeIp {
		// Delete any existing route
		if dcRoute.Gateway.IsValid() {
			err = unix.DelIpRoute(Cfg.DataCenter.Cidr, dcRoute.Gateway)
			if err != nil {
				return err
			}
		}
		err = unix.AddIpRoute(Cfg.DataCenter.Cidr, minikubeIp)
		if err != nil {
			return err
		}
	}
	minikubeRoute, err := unix.GetIpRoute(minikubeIp)
	if err != nil {
		return err
	}
	searchDomains := []string{
		Cfg.DataCenter.Name + ".default.svc." + Cfg.DnsDomain,
		"wikimedia" + ".default.svc." + Cfg.DnsDomain,
	}
	err = unix.SplitDns(corednsIp, minikubeRoute.Dev, searchDomains)
	if err != nil {
		return err
	}
	// HACK: is it always a /24?
	minikubeCidr, err := minikubeIp.Prefix(24)
	if err != nil {
		return err
	}
	fmt.Println("Exporting source code for the puppetserver via NFS.")
	err = unix.ExportNfs(Cfg.SourceCode, minikubeCidr)
	if err != nil {
		return err
	}
	restclient, clientset, err := kubeClientset()
	if err != nil {
		return err
	}
	return kubenode.Reconcile([]cfg.Node{}, Prune, restclient, clientset, Cfg)
}
