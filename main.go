package main

import (
	"embed"
	"fmt"
	"os"

	"gitlab.wikimedia.org/jhathaway/dcl/cmd"
)

//go:embed ssh
var dclSshConfig embed.FS

func main() {
	cmd.DclSshConfig = dclSshConfig

	if err := cmd.Execute(); err != nil {
		fmt.Fprintf(os.Stderr, "Error: %v\n", err)
		os.Exit(1)
	}
}
